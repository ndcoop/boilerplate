// NOTE: Comman components of build

var utils = require('./utils');

var BellOnBundlerErrorPlugin = require('bell-on-bundler-error-plugin');
var CommonChunksPlugin      = require('webpack/lib/optimize/CommonsChunkPlugin');
var CopyWebpackPlugin       = require('copy-webpack-plugin');
var ForkCheckerPlugin       = require('awesome-typescript-loader').ForkCheckerPlugin;
var HtmlWebpackPlugin       = require('html-webpack-plugin');
var OccurrenceOrderPlugin    = require('webpack/lib/optimize/OccurenceOrderPlugin');

var autoprefixer = require('autoprefixer');

// var title = utils.manifest('productName');
var METADATA = {
  title: 'Boilerplate',
  version: '1.0.0',
  baseUrl: '/'
};

module.exports = {

  /**
   * Static metadata of index.html
   */
  metadata: METADATA,

  /**
   * entry
   * The entry point for the bundle
   *
   * see:  http://webpack.github.io/docs/configuration.html#entry
   */
  entry: {
    'polyfills': './src/polyfills.ts',
    'vendor': './src/vendor.ts',
    'app': ['bootstrap-loader', './src/main.browser.ts']
  },

  /**
   * module
   * Options affecting the normal modules
   *
   * see:  http://webpack.github.io/docs/configuration.html#module
   */
  module: {

    /**
     * module.loaders
     * An array of automatically applied loaders
     *
     * see:  http://webpack.github.io/docs/configuration.html#module-loaders
     */
     loaders: [

       // https://github.com/s-panferov/awesome-typescript-loader
       // Awesome Typescript loader for webpack
       { test: /\.ts$/, exclude: [ /node_modules/, /\.(spec.e2e)\.ts$/ ], loader: 'awesome-typescript-loader' },

       { test: /\.html$/, exclude: /node_modules/, loader: 'raw-loader' },

       { test: /\.css$/, loaders: [ 'style', 'css', 'postcss' ] },

       // https://github.com/jtangelder/sass-loader
       // SASS loader for webpack
       { test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader'] },

       { test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000' },

       { test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/, loader: 'file-loader' },

       { test: /bootstrap-sass\/assets\/javascripts\//, loader: 'imports?jQuery=jquery' }

     ],

     /**
      * preLoaders
      * Syntax like module.loaders.  An array of applied pre and post loaders.
      *
      * see:  http://webpack.github.io/docs/configuration.html#module-preloaders-module-postloaders
      */
     preLoaders: [

       // https://github.com/wbuchwalter/tslint-loader
       // // tslint loader for webpack
       { test: /\.ts$/, exclude: [ utils.root('node_modules') ], loader: 'tslint-loader' },

       // https://github.com/webpack/source-map-loader
       // Extracts SourceMaps for source files that are added as sourceMapping URL comment
       { test: /\.js$/, exclude: [ utils.root('node_modules/ng-metadata') ], loader: 'source-map-loader' }
     ]

  },

  /**
   * resolve
   * Options affecting the resolving of modules
   *
   * see:  http://webpack.github.io/docs/configuration.html#resolve
   */
  resolve: {

    /**
     * root
     * The directory (absolute path) that contains your modules.
     *
     * see:  http://webpack.github.io/docs/configuration.html#resolve-root
     */
    root: utils.root('src'),

    /**
     * modulesDirectories
     * An array of directory names to be resolved to the current directory as well as its ancestors, and searched for modules.
     *
     * see:  http://webpack.github.io/docs/configuration.html#resolve-root
     */
    modulesDirectories: ['node_modules'],

    /**
     * extensions
     * An array of extensions that should be used to resolve modules
     *
     * see:  http://webpack.github.io/docs/configuration.html#resolve-extensions
     */
    extensions: ['', '.ts', '.js', '.webpack.js', '.web.js']

  },

  /**
   * devtool
   * Choose a developer tool to enhance debugging
   * source-map:  A SourceMap is emitted.  See also output.sourceMapFilename
   *
   * see:  http://webpack.github.io/docs/configuration.html#devtool
   */
  devtool:  'source-map',

  /**
   * node
   * Include polyfills or mocks for various node stuff
   *
   * see:  http://webpack.github.io/docs/configuration.html#node
   */
  node: {
    global: 'window',
    crypto: 'empty',
    module: false,
    clearImmediate: false,
    setImmediate: false
  },

  /**
   * plugins
   * Add additional plugins to the compiler
   *
   * see: http://webpack.github.io/docs/configuration.html#plugins
   */
  plugins: [

    /**
     * BellOnBunlerErrorPlugin
     * Get notification on bundler build errors
     */
    new BellOnBundlerErrorPlugin(),

    /**
     * CommonChunksPlugin
     * Webpac:  optimize
     *
     * shares common code between modules
     *
     * see:  http://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
     */
    new CommonChunksPlugin({

      // The chunk name of the common chunks
      name: utils.reverse(['polyfills', 'vendor', 'app']),

      // The minmum number of chunks which need to contain a module before it's
      // moved into the common chunks.
      minChunks: Infinity

    }),

    /**
     * CopyWebpackPlugin
     * Copy files and directories in webpack
     *
     * see:  https://github.com/kevlened/copy-webpack-plugin
     */
    new CopyWebpackPlugin([{
      from: 'src/assets',
      to: 'assets'
    }]),

    /**
     * ForkCheckerPlugin
     * Do type checking in a spearate process, so webpack doesn't need to wait.
     *
     * see:  https://github.com/s-panferov/awesome-typescript-loader
     */
    new ForkCheckerPlugin(),

    /**
     * HtmlWebpackPlugin
     * Simplifies creation of HTML files to serve your webpack bundles
     *
     * see:  https://github.com/ampedandwired/html-webpack-plugin
     */
    new HtmlWebpackPlugin({

      // the title to use for the generated HTML document
      // title:  METADATA.title,

      // Path to the template
      template: 'src/index.html',

      // Inject all assets into the given template or templateContent
      inject: 'body',

      // Allow to control how chunks should be sorted before they are included to the html
      chunksSortMode: utils.packageSort(['polyfills', 'vendor', 'app'])

    }),

  /**
     * [OccurrenceOrderPlugin description]
     * @preferEntry {true} give entry chunks higher priority.  This makes
     * chunks smaller but increases the overall size
     */
    new OccurrenceOrderPlugin(true)

  ],

  postcss: [autoprefixer]

};
