Error.stackTraceLimit = Infinity;

require('../src/polyfills.ts');
require('../src/vendor.ts');

require('angular-mocks/ngMock');

var testContext = require.context('../src', true, /\.spec\.ts/);

// get all the files, for each file, call the context function
// that will require the file and load it up here. Context will
// loop and require those spec files here
function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

var modules = requireAll(testContext);
// requires and returns all modules that match
