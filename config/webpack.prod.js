// NOTE: Production components of build

var utils = require('./utils');

var config = require('./webpack.common');
var merge  = require('webpack-merge');

var DedupePlugin   = require('webpack/lib/optimize/DedupePlugin');
var DefinePlugin   = require('webpack/lib/DefinePlugin');
var ProvidePlugin  = require('webpack/lib/ProvidePlugin');
var UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
var WebpackMd5Hash = require('webpack-md5-hash');

var ENV = process.env.ENV = process.env.NODE_ENV = 'production';
var METADATA = merge(config.metadata, {
  ENV: ENV,
  HRM: false
});

module.exports = merge(config, {

  /**
   * Custom Attributes
   *
   * Metadata of index.html
   */
  metadata: METADATA,

  /**
   * Custom Attributes
   *
   * tslint-loader configuration
   */
  tslint: {
    emitErrors: true,
    failOnHint: true,
    resourcePath: 'src'
  },

  /**
   * output
   * Options affectin the ouptu of the compilation
   */
  output: {

    /**
     * filename
     * specifies the name of each output file on disk
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-filename
     */
    filename: '[name].[chunkhash].bundle.js',

    /**
     * path
     * The ouput directory as absolute path
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-path
     */
    path: utils.root('dist'),

    /**
     * chunkFilename
     * The filname of the non-entry chunks as relative path inside the output.path directory
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-chunkfilename
     */
    chunkFilename: '[id].[chunkhash].chunk.js',

    /**
     * sourceMapFilename
     * The filename of the SourceMaps for the JavaScript files
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-sourmapfilename
     */
    sourceMapFilename: '[name].[chunkhash].bundle.map'

  },

  /**
   * cache
   * Cache generated modules and chunks to improve performance for multiple builds
   *
   * see:  http://webpack.github.io/docs/configuration.html#cache
   */
  cache: false,

  /**
   * debug
   * switch loaders to debug mode
   *
   * see:  http://webpack.github.io/docs/configuration.html#debug
   */
  debug: false,

  /**
   * plugins
   * Add additional plugins to the compiler
   *
   * see: http://webpack.github.io/docs/configuration.html#plugins
   */
  plugins: [

    /**
     * DedupePlugin
     * Webpack: optimize
     *
     * Prevents the inclusion of duplicat code in your bundle and instead
     * applies a copy of the function at runtime
     *
     * see:  https://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new DedupePlugin(),

    /**
     * DefinePlugin
     * Webpack:  Dependency Injection
     *
     * Define free variables
     *
     * see:  http://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new DefinePlugin({
      'ENV': JSON.stringify(METADATA.ENV),
      'HMR': METADATA.HMR,
      'process.env': {
        'ENV': JSON.stringify(METADATA.ENV),
        'NODE_ENV': JSON.stringify(METADATA.ENV),
        'HMR': METADATA.HMR
      }
    }),

    /**
     * UglifyJsPlugin
     * Minimze all JavaScript output of chunks.  Loaders are switch into
     * minimizing mode
     *
     * See:  See: https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
     */
    new UglifyJsPlugin({

      beautify: false, //production

      mangle: {

        screw_ie8: true,

        keep_fnames: true

      }, //production

      compress: {

        screw_ie8: true

      }, //production

      comments: false //production

    })

  ]

});
