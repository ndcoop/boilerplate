context
entry
output
* filename
* path
* publicPath
* chunkFilename
* sourceMapFilename
* ~~devtoolModuleFilenameTemplate~~
* ~~devtoolFallbackModuleFilenameTemplate~~
* ~~devtoolFallbackModuleFilenameTemplate~~
* ~~hotUpdateChunkFilename~~
* ~~hotUpdateMainFilename~~
* ~~jsonpFunction~~
* ~~hotUpdateFunction~~
* ~~pathinfo~~
* ~~ibrary~~
* ~~libraryTarget~~
* ~~umdNamedDefine~~
* ~~sourcePrefix~~
* ~~crossOriginLoading~~
module
* preLoaders
* loaders
* postLoaders
* noParse
* xxxContextXxx
  * xxx
    * expr
    * wrapped
    * unknown
  * Xxx
    * Request
    * Recursive
    * RegExp
    * Critical
resolve
* alias
* root
* moduleDirectories
* ~~fallback~~
* extensions
* ~~packageMains~~
* ~~packageAlias~~
* ~~unsafeCache~~
resolveLoader
* moduleTemplates
externals
target
bail
profile
cache
debug
devtool
devServer
node
amd
loaders
recordsPath
recordsInputPath
recordsOutputPath
plugins
