// NOTE: Test components of build

var utils = require('./utils');

var config = require('./webpack.common');
var merge  = require('webpack-merge');

var DefinePlugin       = require('webpack/lib/DefinePlugin');

var ENV = process.env.ENV = process.env.NODE_ENV = 'test';
var METADATA = merge(config.metadata, {
  ENV: ENV,
  HMR: false
});

var testConfig = merge(config, {

  /**
   * Custom Attributes
   *
   * tslint-loader configuration
   */
  tslint: {
    emitErrors: true,
    failOnHint: false,
    resourcePath: 'src'
  },

  metadata: METADATA,

  entry: {},

  output: {},

  /**
   * module
   * Options affecting the normal modules
   *
   * see:  http://webpack.github.io/docs/configuration.html#module
   */
  module: {

    loaders: [

      // https://github.com/s-panferov/awesome-typescript-loader
      // Awesome Typescript loader for webpack
      { test: /\.ts$/, exclude: [ /node_modules/ ], loader: 'awesome-typescript-loader' },

    ],

    /**
     * postLoaders
     * Syntax like module.loaders.  An array of applied pre and post loaders.
     *
     * see:  http://webpack.github.io/docs/configuration.html#module-preloaders-module-postloaders
     */
    postLoaders: [

      // https://github.com/deepsweet/istanbul-instrumenter-loader
      // instrument js file with Istanbul for subsequnet code coverage and reporting
      {
        test: /\.(js|ts)$/,
        include: utils.root('src'),
        exclude: [
          /node_modules/,
          /\.(e2e|spec)\.ts$/,
          /(polyfills|vendor)\.ts$/
        ],
        loader: 'istanbul-instrumenter-loader'
      }

    ]

  },

  /**
   * devtool
   * Choose a developer tool to enhance debugging
   * source-map:  A SourceMap is emitted.  See also output.sourceMapFilename
   *
   * see:  http://webpack.github.io/docs/configuration.html#devtool
   */
  devtool:  'inline-source-map',

  node: {
    process: false
  }

});

testConfig.plugins = [

    /**
     * DefinePlugin
     * Webpack:  Dependency Injection
     *
     * Define free variables
     *
     * see:  http://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new DefinePlugin({
      'ENV': JSON.stringify(METADATA.ENV),
      'HMR': METADATA.HMR,
      'process.env': {
        'ENV': JSON.stringify(METADATA.ENV),
        'NODE_ENV': JSON.stringify(METADATA.ENV),
        'HMR': METADATA.HMR
      }
    })

];

module.exports = require('webpack-validator')(testConfig);
