// NOTE: Development components of build

var utils = require('./utils');

var config = require('./webpack.common');
var merge  = require('webpack-merge');

var DefinePlugin   = require('webpack/lib/DefinePlugin');
var NoErrorsPlugin = require('webpack/lib/NoErrorsPlugin');

var ENV = process.env.ENV = process.env.NODE_ENV = 'development';
var HMR = utils.hasProcessFlag('hot');
var METADATA = merge(config.metadata, {
  host: 'localhost',
  port: '9324',
  ENV: ENV,
  HRM: HMR
});

var devConfig = merge(config, {

  /**
   * Custom Attributes
   *
   * Static metadata of index.html
   */
  metadata: METADATA,

  /**
   * Custom Attributes
   *
   * tslint-loader configuration
   */
  tslint: {
    emitErrors: false,
    failOnHint: false,
    resourcePath: 'src'
  },

  /**
   * output
   * Options affectin the ouptu of the compilation
   */
  output: {

    /**
     * filename
     * specifies the name of each output file on disk
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-filename
     */
    filename: '[name].bundle.js',

    /**
     * path
     * The ouput directory as absolute path
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-path
     */
    path: utils.root('dist'),

    /**
     * publicPath
     * Specefies the public URL address of the output file when referenced
     * in the browser
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-publicPath
     */
    publicPath: '/',

    /**
     * chunkFilename
     * The filname of the non-entry chunks as relative path inside the output.path directory
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-chunkfilename
     */
    chunkFilename: '[id].chunk.js',

    /**
     * sourceMapFilename
     * The filename of the SourceMaps for the JavaScript files
     *
     * see:  http://webpack.github.io/docs/configuration.html#output-sourmapfilename
     */
    sourceMapFilename: '[name].map'

  },

  /**
   * cache
   * Cache generated modules and chunks to improve performance for multiple builds
   *
   * see:  http://webpack.github.io/docs/configuration.html#cache
   */
  cache: false,

  /**
   * debug
   * switch loaders to debug mode
   *
   * see:  http://webpack.github.io/docs/configuration.html#debug
   */
  debug: true,

  /**
   * devServer
   */
  devServer: {
    historyApiFallback: {
      index: '/'
    },
    port: METADATA.port
  },

  /**
   * plugins
   * Add additional plugins to the compiler
   *
   * see: http://webpack.github.io/docs/configuration.html#plugins
   */
  plugins: [

    /**
     * DefinePlugin
     * Webpack:  Dependency Injection
     *
     * Define free variables
     *
     * see:  http://webpack.github.io/docs/list-of-plugins.html#defineplugin
     */
    new DefinePlugin({
      'ENV': JSON.stringify(METADATA.ENV),
      'HMR': METADATA.HMR,
      'process.env': {
        'ENV': JSON.stringify(METADATA.ENV),
        'NODE_ENV': JSON.stringify(METADATA.ENV),
        'HMR': METADATA.HMR
      }
    }),

    /**
     * NoErrorsPlugin
     * Webpack:  Other
     *
     * When there are errors while compiling this plugin skips the emitting
     * phase and recording phase, so there are no assets emitted the Include
     * errors.
     *
     * see:  http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
     */
    new NoErrorsPlugin()

  ]

});

module.exports = require('webpack-validator')(devConfig);
