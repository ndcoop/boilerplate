export default angular.module('app.router', [])
  .config([
    '$locationProvider',
    '$urlRouterProvider',
    ($locationProvider, $urlRouterProvider) => {
      $locationProvider.html5Mode({ enabled: true, requireBase: false});
      $urlRouterProvider.otherwise('/');
    }
  ]);
