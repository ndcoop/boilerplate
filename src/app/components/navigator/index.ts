import {provide} from 'ng-metadata/core';

import {NavComponent} from './nav.component';

export const NavModule = angular
.module('app.components.nav', [])
.directive(...provide(NavComponent))
.name;
