import {Component} from 'ng-metadata/core';

@Component({
  selector: 'navigator',
  template: `
    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Boilerplate</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a ui-sref="index">Home</a></li>
          <li><a ui-sref="about">About</a></li>
        </ul>
      </div>
    </div>
  `
})
export class NavComponent {}
