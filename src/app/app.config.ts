StateConfig.$inject = ['$locationProvider'];

export function StateConfig($locationProvider: ng.ILocationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
}
