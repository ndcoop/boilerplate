import {Component} from 'ng-metadata/core';

import {NavModule} from './components';

@Component({
  selector: 'app',
  directives: [],
  template: `
    <navigator></navigator>
    <div class="container">
      <h1>Hello, world</h1>
      <ui-view></ui-view>
    </div>
  `
})
export class AppComponent { }
