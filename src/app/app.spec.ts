import {AppModule} from './index';
import {AppComponent} from './app.component';
import {StateConfig} from './app.config';

import {renderFactory, IRender} from 'ng-metadata/testing';
import {getInjectableName} from 'ng-metadata/core';

let render: IRender;

describe('App Module', () => {

  describe('Component', () => {

    beforeEach(angular.mock.module(AppModule));
    beforeEach(angular.mock.inject(($injector: ng.auto.IInjectorService) => {
      const $compile = $injector.get<ng.ICompileService>('$compile');
      const $rootScope = $injector.get<ng.IRootScopeService>('$rootScope');
      const $scope = $rootScope.$new();

      render = renderFactory($compile, $scope);
    }));

    it('should render', () => {
      const {ctrl, compiledElement} = render(AppComponent);
      
      expect(ctrl instanceof AppComponent).toBe(true);
    });
  });

});
