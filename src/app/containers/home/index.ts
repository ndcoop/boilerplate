import {provide} from 'ng-metadata/core';

import {HomeComponent} from './home.component';
import {StateConfig} from './home.config';

export const HomeContainer = angular
.module('app.containers.home', [])
.config(StateConfig)
.directive(...provide(HomeComponent))
.name;
