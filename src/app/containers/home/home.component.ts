import {Component, Inject, AfterViewInit} from 'ng-metadata/core';

@Component({
  selector: 'home',
  template: `
    <h1>Home</h1>
  `
})
export class HomeComponent implements AfterViewInit {
  constructor(@Inject('$log') private _$log: ng.ILogService) {}

  ngAfterViewInit() {
    this._$log.log('Home container is fully initialized');
  }
}
