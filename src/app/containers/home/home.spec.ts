import {HomeContainer} from './index';
import {HomeComponent} from './home.component';
import {StateConfig} from './home.config';

import {renderFactory, IRender} from 'ng-metadata/testing';
import {getInjectableName} from 'ng-metadata/core';

let render: IRender;

describe('App Module', () => {

  describe('Component', () => {

    beforeEach(angular.mock.module(HomeContainer));
    beforeEach(angular.mock.inject(($injector: ng.auto.IInjectorService) => {
      const $compile = $injector.get<ng.ICompileService>('$compile');
      const $rootScope = $injector.get<ng.IRootScopeService>('$rootScope');
      const $scope = $rootScope.$new();

      render = renderFactory($compile, $scope);
    }));

    it('should render', () => {
      const {ctrl, compiledElement} = render(HomeComponent);

      expect(ctrl instanceof HomeComponent).toBe(true);
    });
  });

});
