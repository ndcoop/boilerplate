StateConfig.$inject = ['$stateProvider'];

export function StateConfig($stateProvider: angular.ui.IStateProvider) {

  $stateProvider
    .state('index', {
      url: '/',
      template: '<home></home>',
      controllerAs: '$home'
    });

}
