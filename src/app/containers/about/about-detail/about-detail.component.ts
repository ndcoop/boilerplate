import {Component, Inject, AfterViewInit} from 'ng-metadata/core';

import {DetailModel, AboutService} from '../../../services';

@Component({
  selector: 'about-detail',
  template: `
    <h3>About Detail</h3>
    <p>{{$ctrl.detail}}</p>
  `
})
export class AboutDetailComponent implements AfterViewInit {
  detail: string;
  detailId: string;

  constructor(
    private aboutService: AboutService,
    @Inject('$log') private _$log,
    @Inject('$stateParams') private _$stateParmas
  ) {
    this.detailId = this._$stateParmas.id;
    this.detail = this.aboutService.detail(this.detailId);
  }

  ngAfterViewInit() {
    this._$log.log(this.detail);
  }
}
