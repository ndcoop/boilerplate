import {provide} from 'ng-metadata/core';

import {AboutComponent} from './about.component';
import {StateConfig} from './about.config';

import {AboutDetailComponent} from './about-detail';

export const AboutContainer = angular
.module('app.containers.about', [])
.config(StateConfig)
.directive(...provide(AboutComponent))
.directive(...provide(AboutDetailComponent))
.name;
