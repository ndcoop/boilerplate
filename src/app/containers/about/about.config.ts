StateConfig.$inject = ['$stateProvider'];

export function StateConfig($stateProvider: angular.ui.IStateProvider) {

  $stateProvider

    .state('about', {
      url: '/about',
      template: '<about></about>'
    })

    .state('about.detail', {
      url: '/:id',
      template: '<about-detail></about-detail>'
    });

}
