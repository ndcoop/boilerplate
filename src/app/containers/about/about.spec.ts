import {AboutContainer} from './index';
import {AboutComponent} from './about.component';

import {renderFactory, IRender} from 'ng-metadata/testing';
import {getInjectableName} from 'ng-metadata/core';

let $compile: ng.ICompileService;
let $rootScope: ng.IRootScopeService;
let $scope;
let render: IRender;

describe('About Container', () => {

  describe('Component', () => {

    beforeEach(() => {
      angular.mock.module(AboutContainer);
    });

    beforeEach(angular.mock.inject((_$injector_: ng.auto.IInjectorService) => {
      const $injector = _$injector_;

      $compile  = $injector.get<ng.ICompileService>('$compile');
      $rootScope = $injector.get<ng.IRootScopeService>('$rootScope');
      $scope = $rootScope.$new();

      render = renderFactory($compile, $scope);
    }));

    it('should render', () => {
       const {ctrl} = render(AboutComponent);
      expect(ctrl instanceof AboutComponent).toBe(true);
    });
  });

});
