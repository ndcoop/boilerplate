import {Component, Inject, OnInit, AfterViewInit} from 'ng-metadata/core';
import {AboutModel, AboutService} from '../../services';

@Component({
  selector: 'about',
  template: `
    <h2>About</h2>
    <ul class="nav nav-pills nav-justified">
      <li role="presentation"
          ng-repeat="item in $ctrl.items">
        <a ui-sref="about.detail({id: item.id})"
           ui-sref-active="active"
           ng-bind="item.name">
        </a>
      </li>
    </ul>
    <div class="jumbotron">
      <ui-view></ui-view>
    </div>
  `
})
export class AboutComponent implements AfterViewInit {
  items: AboutModel;

  constructor(
    private aboutService: AboutService,
    @Inject('$log') private _$log: ng.ILogService
  ) {
    this.items = this.aboutService.list;
  }

  ngAfterViewInit() {
    this._$log.log('About container is fully initialized');
  }

}
