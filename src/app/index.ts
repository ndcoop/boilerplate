import {provide} from 'ng-metadata/core';

import {AppComponent} from './app.component';
import {StateConfig} from './app.config';

import {
  NavModule
} from './components';

angular.module('app.components', [
  NavModule
]);

import {
  AboutContainer,
  HomeContainer
} from './containers';

angular.module('app.containers', [
  AboutContainer,
  HomeContainer
]);

import {
  AboutService
} from './services';

export const AppModule = angular
.module('app', [
  'ui.router',
  'app.components',
  'app.containers'
])
.config(StateConfig)
.directive(...provide(AppComponent))
.service(...provide(AboutService))
.name;
