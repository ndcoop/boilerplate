import {Injectable} from 'ng-metadata/core';

export type DetailModel = {
  description: string
};

export type AboutModel = {
  id: string,
  name: string,
  description?: string
};

@Injectable()
export class AboutService {

  private _list: AboutModel[] = [
    {
      id: 'angular',
      name: 'Angular',
      description: 'Framework'
    },
    {
      id: 'angular-ui-router',
      name: 'Angular UI Router',
      description: 'States'
    },
    {
      id: 'ng-metadata',
      name: 'Ng Metadata',
      description: 'Angular decorators'
    },
    {
      id: 'bootstrap',
      name: 'Bootstrap',
      description: 'UI Framework'
    }
  ];

  get list() {
    return angular.copy(this._list);
  }

  detail(value) {
    let obj = this._list.filter((item) => item.id === value);
    return obj[0].description;
  }
}
