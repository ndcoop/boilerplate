describe('About Servce', () => {

  beforeEach(module('app'));

  let chimp;
  let $log;

  beforeEach(inject((_AboutService_, _$log_) => {
    chimp = _AboutService_;
    $log = $log;
  }));

  describe('when invoked', () => {

    beforeEach(() => {
      chimp.detail('ng-metadata');
    });

    it ('should return detail', () => {
      expect($log.warn.args[0][0]).toEqual('Angular decorators');
    })
  })
})
