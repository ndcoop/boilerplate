import 'es5-shim';
import 'es6-shim';
import 'es6-promise';

import 'reflect-metadata';

// import 'core-js/es6';
// import 'core-js/es7/reflect';
// require('zone.js/dist/zone');

if ('production' === ENV) {
  // Production

} else {
  // Development

  Error.stackTraceLimit = Infinity;
  require('ts-helper');
  require('ts-helpers');
  // require('zone.js/dist/long-stack-trace-zone');
}
